<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Adminhtml_Gls_CsvController extends Mage_Adminhtml_Controller_Action
{
	protected function _isAllowed()
	{
		return Mage::getSingleton('admin/session')->isAllowed('gls/csv');
	}
	
	public function downloadAction()
	{
		$params = $this->getRequest()->getParams();

		if(isset($params['file'])){
			$file = $params['file'];
		}
		else{
			exit();
		}

		$fileName = 'GLS-' . $file;

		$this->_prepareDownloadResponse($fileName, null, 'application/octet-stream', filesize(Mage::getBaseDir("var") . DS . "GLS" . DS . $file));

		$this->getResponse()->sendHeaders();

		$this->output($file);
		exit();
	}

	public function output($filename)
	{
		$ioAdapter = new Varien_Io_File();
		$ioAdapter->open(array('path' => Mage::getBaseDir("var") . DS . "GLS"));

		$ioAdapter->streamOpen($filename, 'r');
		while ($buffer = $ioAdapter->streamRead()) {
			echo $buffer;
		}
		$ioAdapter->streamClose();
	}
	
	public function createcsvfilesAction()
	{	
		
		$params = $this->getRequest()->getParams();
		if(isset($params['order_ids']))
		{
			
			
			$order_ids = $params['order_ids'];
			$export_filename = false;
			if(count($order_ids) > 1) {
				$export_filename = "gls_orders_exported_".Mage::getModel('core/date')->date('Y-m-d_H-i_s').".csv";
			}
			foreach($order_ids as $order_id)
			{
				$order = Mage::getModel('sales/order')->load($order_id);
				Mage::getModel('gls/observer')->exportToGLSFormat($order,$export_filename);
			}
			Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/index/',$params));
		}
		elseif(isset($params['order_id']))
		{
			$order_id = $params['order_id'];
			$order = Mage::getModel('sales/order')->load($order_id);
			Mage::getModel('gls/observer')->exportToGLSFormat($order);
			Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/view/',$params));
		}
		else
		{
			Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/index/',$params));
		}
		
		
	}
	
}
?>