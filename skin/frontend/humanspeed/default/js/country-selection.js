/**
 * Magento
 *
 * Country Selection
 *
 * Handles the Country Selection popup and the associated
 * cookie logic
**/

jQuery(document).ready(function($) {

	/**********************************************************
	* GETTER COOKIE
	*
	* Attempts to get a cookie value by the cookie name
	* @string - return - the value of the cookie or an emtpy string if
	* nothing is found
	***********************************************************/
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
		}
		return "";
	}

	/**********************************************************
	* SETTER COOKIE
	*
	* Function to set cookie values
	* @string - cname - name of cookie
	***********************************************************/
	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+d.toUTCString();
		var path = "path=/";
		var domain = "domain=.humanspeed.dk";

		console.log('setting cookie ' + cvalue );

		document.cookie = cname + "=" + cvalue + ";" + expires + ";" + path + ";" + domain;
	}


	/**********************************************************
	* Popup Show/Hide
	***********************************************************/
	//Hide popup
	function hideCountrySelect() {
		$('#country-select').fadeOut(200, function() {
			$('#shield').fadeOut(200);
			$('#country-select').data( 'visible') ===  false;
		});
	}

	//Show popup
	function showCountrySelect() {
		$('#country-select').fadeIn(200, function() {
			$('#shield').fadeIn(200);
			$('#country-select').data( 'visible') ===  true;
		});
	}

	/**********************************************************
	* Read Cookies
	***********************************************************/
	//Read selected site cookie
	//Returns the store code if set, else null
	function getSelectedWebsite() {
		var selectedWebsiteVal = getCookie( 'selectedWebsiteCookie' );

		if( selectedWebsiteVal == "" ) {
			return null;
		}
		else {
			return selectedWebsiteVal;
		}
	}

	if( getSelectedWebsite() ) {
		console.log('Website is set and is ' + getSelectedWebsite() );
	}
	else if( getSelectedWebsite() == null && !window.location.href.match(/local/)) {
		showCountrySelect();
		console.log('Website not set yet');
	}

	/**********************************************************
	* Clickhandler
	***********************************************************/
	$('.country-select li').on('click', function() {
		//Get country from ID
		countryCode = $(this).attr('id');

		//console.log( countryCode );

		//Set Cookie
		setCookie( 'selectedWebsiteCookie', countryCode, 30 );

		//Hide Popup
		hideCountrySelect();
	});

});
