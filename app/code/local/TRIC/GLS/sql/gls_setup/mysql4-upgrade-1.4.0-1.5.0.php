<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */


$installer = $this;
$installer->startSetup();

$table = $installer->getTable('sales_flat_order');
try {
	$installer->run("ALTER TABLE $table ADD gls_privat_kommentar varchar(255);");
} catch(Exception $e) {}

$installer->endSetup();