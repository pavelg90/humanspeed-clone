<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2015 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */


class TRIC_Info_Block_System_Config_Form_Fieldset_Tricinfo_Extensions extends Mage_Adminhtml_Block_System_Config_Form_Fieldset
{
	protected $_formField;

	public function render(Varien_Data_Form_Element_Abstract $element)
	{
		$html = $this->_getHeaderHtml($element);
		$modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
		sort($modules);

		$excludedTRICModules = array('TRIC_Info','TRIC_BlankSeo','TRIC_Adminhtml');

		if (!(Mage::app()->loadCache('tric_info_extensions_feed')) || (time() - Mage::app()->loadCache('tric_info_extensions_feed_lastcheck')) > 24*60*60) {
			try{
				Mage::getModel('tric_info/feed_extensions')->check();
			}
			catch(Exception $e){
			}
		}

		if ($extensionFeed = Mage::app()->loadCache('tric_info_extensions_feed')) {
			$extensionFeed = unserialize($extensionFeed);
		}

		foreach ($modules as $moduleName) {
			if (strstr($moduleName,'TRIC_') === false) {
				continue;
			}

			if(in_array($moduleName, $excludedTRICModules)){
				continue;
			}

			$html.= $this->_getFieldHtml($element, $moduleName, $extensionFeed);
		}
		$html .= $this->_getFooterHtml($element);

		return $html;
	}

	protected function _getFormField()
	{
		if (empty($this->_formField)) {
			$this->_formField = Mage::getBlockSingleton('adminhtml/system_config_form_field');
		}
		return $this->_formField;
	}

	protected function _getFieldHtml($fieldset, $tricName, $extensionFeed)
	{
		$configData = $this->getConfigData();

		$extension = $tricName;

		$path = 'advanced/modules_disable_output/' . $tricName;
		$data = isset($configData[$path]) ? $configData[$path] : array();

		$ver = Mage::getConfig()->getModuleConfig($tricName)->version;
		$id = $tricName;

		$update = '';
		$readme = '<span style="margin:0 8px;">&nbsp;</span>';

		$hasUpdate = false;

		if ($extensionFeed && isset($extensionFeed[$tricName])) {

			$name = $extensionFeed[$tricName]['display_name'];
			$version = $extensionFeed[$tricName]['version'];
			$url = $extensionFeed[$tricName]['url'];
			$guide = $extensionFeed[$tricName]['guide'];

			if(Mage::app()->getLocale()->getLocaleCode() == 'da_DK'){
				$name = $extensionFeed[$tricName]['display_name_dk'];
				$url = $extensionFeed[$tricName]['url_dk'];
			}

			$tricName = $name;
			if($url){
				$tricName = '<a href="' . $url . '" target="_blank" title="' . $name . '">' . $name . "</a>";
			}

			$readme = '<span style="margin:0 8px;">&nbsp;</span>';
			if($guide){
				$readme = '<a style="margin:0 4px;" href="'.$guide.'" target="_blank"><img src="'. $this->getSkinUrl('tric_info/images/readme.png').'"/></a>';
			}

			if($ver && $version){
				if ($this->_versionAsNumber($ver) < $this->_versionAsNumber($version)) {
					$update = '<a href="' . $url . '" target="_blank"><img src="' . $this->getSkinUrl('tric_info/images/update.gif') . '" title="' . $this->__("Update available") . '"/></a>';
					$hasUpdate = true;
					$tricName = "$update $tricName";
				}
			}
		}

		if (!$hasUpdate) {
			$update = '<img src="' . $this->getSkinUrl('tric_info/images/ok.gif') . '" title="' . $this->__("Installed") . '"/>';
			$tricName = "$update $readme $tricName";
		}

		$validDomains = array();
		if(!in_array($extension, array('TRIC_EasyRedirects','TRIC_Speedup'))){
			$validDomains = Mage::helper("tric_info")->getValidDomains($extension);
		}
		$domains = "";
		if(count($validDomains) == 1)
		{
			$domains = '<strong>'.$this->__("Valid for the following domain:").'</strong><br/>';
		}
		if(count($validDomains) > 1)
		{
			$domains = '<strong>'.$this->__("Valid for the following domains:").'</strong><br/>';
		}

		foreach($validDomains as $domain)
		{
			$domains .= "<div>$domain</div>";
		}

		if ($ver) {
			$field = $fieldset->addField($id, 'label',
				array(
					'name' => '',
					'label' => $tricName,
					'value' => $ver,
					'scope' => true,
					'scope_label' => $domains
				))->setRenderer($this->_getFormField());
			return $field->toHtml();
		}
		return '';

	}

	protected function _versionAsNumber($v)
	{
		$version = str_replace(".","",$v);
		while(strlen($version) < 4)
		{
			$version = $version*10;
		}
		return $version;
	}
}
