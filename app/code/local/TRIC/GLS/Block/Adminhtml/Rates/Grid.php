<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Block_Adminhtml_Rates_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {		
        $helper = $this->_getHelper();
        parent::__construct();
        $this->setId('glsGrid');
        $this->setDefaultSort('pk');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->_exportPageSize = 10000;
        $this->setEmptyText($helper->__('No shipping rates found.'));
    }

    public function getWebsiteId()
    {
        return $this->_getHelper()->getWebsiteId($this->getWebsite());
    }

    protected function _getHelper()
    {
        return Mage::helper('gls/rates');
    }

    protected function _prepareCollection()
    {
        $websiteId = $this->getWebsiteId();
        $collection = Mage::getModel('gls/rate')->getCollection();
        
        $select = $collection->getSelect();
        $select->where('active = 1');
        
        if ($websiteId) $select->where('website_id = ?', $websiteId);
        else $select->where('website_id = -1');
        
        $this->setCollection($collection);
        $collection = parent::_prepareCollection();

        return $collection;

    }
    
    protected function getDestCountryOptions()
    {
        $helper = $this->_getHelper();
        $options = array();
        $countries = Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(false);
        if (isset($countries[0]) && !$countries[0]['value']) 
        {
	        //$countries[0] = array('value' => '*', 'label' => '*', );
	        unset($countries[0]);
        }
        
        foreach ($countries as $country) {
            $options[$country['value']] = $country['label'];
        }
        return $options;
    }
    
    protected function getConditionNameOptions()
    {
        $options = array();
        $names = Mage::getModel('adminhtml/system_config_source_shipping_tablerate')->toOptionArray();
        foreach ($names as $name) {
            $options[$name['value']] = $name['label'];
        }
        return $options;
    }
    
    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
    
    protected function _filterCountryCondition($collection, $column)
	{
		if (!$value = $column->getFilter()->getValue()) {
			return;
		}
		
		$this->getCollection()->addFieldToFilter('dest_country_id', array('like' => "%,$value,%"));
	}

    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $helper = $this->_getHelper();
        $this->addColumn('dest_country_id', array(
            'header'    => $helper->__('Country'), 
            'align'     => 'left', 
            'index'     => 'dest_country_id',
//            'filter'	=> 'gls/adminhtml_rates_grid_column_filter_country', 
			'filter_condition_callback' => array($this, '_filterCountryCondition'),             
			'renderer'	=> 'gls/adminhtml_rates_grid_column_renderer_country',             
            'type'      => 'options', 
            'options'   => $this->getDestCountryOptions(), 
        ));
        $this->addColumn('dest_region', array(
            'header'        => $helper->__('Region/State'), 
            'align'         =>'left',
            'index'         => 'dest_region', 
            'filter_index'  => 'region_table.code',
            'filter'	    => 'gls/adminhtml_rates_grid_column_filter_region',
            'default'       => '*', 
        ));
/*
        $this->addColumn('dest_city', array(
            'header'        => $helper->__('City'), 
            'align'         =>'left', 
            'index'         => 'dest_city', 
            'filter'	    => 'gls/adminhtml_rates_grid_column_filter_city', 
            'renderer'	    => 'gls/adminhtml_rates_grid_column_renderer_city', 
            'default'       => '*', 
        ));
*/
        $this->addColumn('dest_zip', array(
            'header'        => $helper->__('Zip/Postal Code From'), 
            'align'         =>'left', 
            'index'         => 'dest_zip', 
            'filter'	    => 'gls/adminhtml_rates_grid_column_filter_zip', 
            'renderer'	    => 'gls/adminhtml_rates_grid_column_renderer_zip', 
            'default'       => '*', 
        ));
        $this->addColumn('dest_zip_to', array(
            'header'        => $helper->__('Zip/Postal Code To'), 
            'align'         =>'left', 
            'index'         => 'dest_zip_to', 
            'filter'	    => 'gls/adminhtml_rates_grid_column_filter_zip', 
            'renderer'	    => 'gls/adminhtml_rates_grid_column_renderer_zip', 
            'default'       => '*', 
        ));
        $this->addColumn('condition_name', array(
            'header'        => $helper->__('Condition Name'), 
            'align'         => 'left', 
            'index'         => 'condition_name', 
            'type'          => 'options', 
            'options'       => $this->getConditionNameOptions(), 
        ));
        $this->addColumn('condition_from_value', array(
            'header'        => $helper->__('Condition Value From'), 
            'align'         => 'left', 
            'index'         => 'condition_from_value', 
            'type'          => 'number', 
            'default'       => '0', 
        ));
        $this->addColumn('condition_to_value', array(
            'header'        => $helper->__('Condition Value To'), 
            'align'         => 'left', 
            'index'         => 'condition_to_value', 
            'type'          => 'number', 
            'default'       => '0', 
        ));
        $store = $this->_getStore();
        $this->addColumn('price', array(
            'header'        => $helper->__('Price'), 
            'align'         => 'left', 
            'index'         => 'price', 
            'type'          => 'price', 
            'currency_code' => $store->getBaseCurrency()->getCode(), 
            'default'       => '0.00', 
        ));
        $this->addColumn('cost', array(
            'header'        => $helper->__('Cost'), 
            'align'         => 'left', 
            'index'         => 'cost', 
            'type'          => 'price', 
            'currency_code' => $store->getBaseCurrency()->getCode(), 
            'default'       => '0.00', 
        ));
         $this->addColumn('code', array(
            'header'        => $helper->__('Code'), 
            'align'         => 'left', 
            'index'         => 'method_code', 
            'type'          => 'text', 
            'default'       => '', 
        ));
         $this->addColumn('name', array(
            'header'        => $helper->__('Name'), 
            'align'         => 'left', 
            'index'         => 'method_name', 
            'type'          => 'text', 
            'default'       => '', 
        ));
        $this->addColumn('description', array(
            'header'        => $helper->__('Description'), 
            'align'         => 'left', 
            'index'         => 'method_description', 
            'type'          => 'text', 
            'default'       => '', 
        ));
        $this->addColumn('sort_order', array(
            'header'        => $helper->__('Sort Order'), 
            'align'         => 'left', 
            'index'         => 'sort_order', 
            'type'          => 'number', 
            'default'       => '0', 
        ));
        $this->addColumn('external_id', array(
            'header'        => $helper->__('External ID'), 
            'align'         => 'left', 
            'index'         => 'external_id', 
            'type'          => 'text', 
            'default'       => '', 
        ));

        //$this->addExportType('*/*/exportCsv', $helper->__('CSV'));
        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('website' => $this->getWebsiteId(), 'pk' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    protected function _prepareMassaction()
    {
        $helper = $this->_getHelper();
        $this->setMassactionIdField('pk');
        $this->getMassactionBlock()->setFormFieldName('pk');
        $this->getMassactionBlock()->addItem('delete', array(
            'label'       => $helper->__('Delete'), 
            'url'         => $this->getUrl('*/*/massDelete', array('website' => $this->getWebsiteId())), 
            'confirm'     => $helper->__('Are you sure?')
        ));
        return $this;
    }
    


}