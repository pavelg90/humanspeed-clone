<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_GLS_Block_Adminhtml_System_Config_Form_Field_Export extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$this->setElement($element);

		$buttonBlock = $this->getLayout()->createBlock('adminhtml/widget_button');

		$params = array(
			'website' => $buttonBlock->getRequest()->getParam('website')
		);

		$data = array(
			'label'     => Mage::helper('adminhtml')->__('Export CSV'),
			'onclick'   => 'setLocation(\''.Mage::helper('adminhtml')->getUrl("adminhtml/adminhtml_gls_rates/export", $params) . 'shippingMethod/\' + this.parentNode.parentNode.parentNode.parentNode.parentNode.id + \'/conditionName/\' + $(\'carriers_gls_condition_name\').value + \'/gls.csv\' )',
			'class'     => '',
		);

		$html = $buttonBlock->setData($data)->toHtml();

		return $html;
	}
	
	
}
