<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */


$installer = $this;
$installer->startSetup();

$table = $installer->getTable('shipping_gls');

try {
	$installer->run("ALTER TABLE $table CHANGE `dest_country_id`  `dest_country_id` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL");

} catch(Exception $e) {}

try {
	$installer->run("UPDATE $table SET dest_country_id = CONCAT(',',dest_country_id,',') WHERE `dest_country_id` NOT LIKE '%,%' AND `dest_country_id` != '0'");

} catch(Exception $e) {}


$installer->endSetup();