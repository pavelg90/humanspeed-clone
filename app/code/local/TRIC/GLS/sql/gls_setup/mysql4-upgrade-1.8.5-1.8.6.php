<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */


$installer = $this;
$installer->startSetup();

$table = $installer->getTable('salesrule');

try {
	$installer->run("ALTER TABLE $table ADD `gls_specific_shipping_method` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL");

} catch(Exception $e) {}

$installer->endSetup();