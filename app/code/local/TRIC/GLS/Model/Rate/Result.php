<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Model_Rate_Result extends Mage_Shipping_Model_Rate_Result
{
	/**
	 *  Sort rates by price from min to max
	 *
	 *  @return   Mage_Shipping_Model_Rate_Result
	 */
	public function sortRatesByPrice ()
	{
		/*
		if (!is_array($this->_rates) || !count($this->_rates)) {
            return $this;
        }
        foreach ($this->_rates as $i => $rate) {
            $tmp[$i] = $rate->getPrice();
        }

        natsort($tmp);

        foreach ($tmp as $i => $price) {
            $result[] = $this->_rates[$i];
        }

        $this->reset();
        $this->_rates = $result;
		*/
		return $this;
	}
}
