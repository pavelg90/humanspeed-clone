<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

$installer = $this;
$installer->startSetup();

try {
	$table = $installer->getTable('sales_flat_quote_address');
	$installer->run("ALTER TABLE $table CHANGE  `droppoint`  `droppoint` VARCHAR( 32 ) NOT NULL DEFAULT  '0'");
} catch(Exception $e) {
	Mage::log($e->getMessage(),null,'gls.log',true);
}
try {
	$table = $installer->getTable('sales_flat_order_address');
	$installer->run("ALTER TABLE $table CHANGE  `droppoint`  `droppoint` VARCHAR( 32 ) NOT NULL DEFAULT  '0'");
} catch(Exception $e) {
	Mage::log($e->getMessage(),null,'gls.log',true);
}

$installer->endSetup();