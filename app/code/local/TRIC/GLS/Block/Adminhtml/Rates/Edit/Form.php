<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
 
class TRIC_GLS_Block_Adminhtml_Rates_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function getSession()
    {
        return Mage::getSingleton('admin/session');
    }

    protected function _getHelper()
    {
        return Mage::helper('gls/rates');
    }

    protected function getModel()
    {
        return Mage::registry('gls_rate');
    }

    protected function getDestCountryValues()
    {
        $countries = Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(false);
        if (isset($countries[0]) && !$countries[0]['value']) 
        {
	        $countries[0] = array('value' => '*', 'label' => '*', );
	        //unset($countries[0]);
        }
        return $countries;
    }

    protected function getDestRegionValues()
    {
        $regions = array(array('value' => '0', 'label' => '*'));
        $model = $this->getModel();
        $destCountryId = $model->getDestCountryId();
        if ($destCountryId) {
            $regionCollection = Mage::getModel('directory/region')->getCollection()
                ->addCountryFilter($destCountryId);
            $regions = $regionCollection->toOptionArray();
            if (isset($regions[0])) $regions[0]['label'] = '*';
        }
        return $regions;
    }

    protected function getConditionNameValues()
    {
        return Mage::getModel('adminhtml/system_config_source_shipping_tablerate')->toOptionArray();
    }
    
    protected function getCityValue()
    {
        $model = $this->getModel();
        $destCity = $model->getDestCity();
        return (($destCity == '*') || ($destCity == '')) ? '*' : $destCity;
    }
    
    protected function getDestZipValue()
    {
        $model = $this->getModel();
        $destZip = $model->getDestZip();
        return (($destZip == '*') || ($destZip == '')) ? '*' : $destZip;
    }
    
    protected function getDestZipToValue()
    {
        $model = $this->getModel();
        $destZipTo = $model->getDestZipTo();
        return (($destZipTo == '*') || ($destZipTo == '')) ? '*' : $destZipTo;
    }
    
    protected function getTitleValue()
    {
        $model = $this->getModel();
        $title = $model->getData('title');
        return $title;
    }

    protected function _prepareForm()
    {
        $helper = $this->_getHelper();
        $model = $this->getModel();
        $isElementDisabled = false;
        $form = new Varien_Data_Form(array('id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post'));
        $form->setHtmlIdPrefix('gls_rate_');
        $fieldset = $form->addFieldset('main_fieldset', array('legend' => $helper->__('Rate Information')));
        if ($model->getId()) {
            $fieldset->addField('pk', 'hidden', array(
                'name'      => 'pk', 
                'value'     => $model->getId(), 
            ));
        }
        $fieldset->addField('website_id', 'hidden', array(
            'name'       => 'website_id', 
            'value'      => $model->getWebsiteId(), 
        ));
        $fieldset->addField('dest_country_id', 'multiselect', array(
            'name'       => 'dest_country_id', 
            'label'      => $helper->__('Country'), 
            'title'      => $helper->__('Country'), 
            'required'   => true, 
            'value'		 => $model->getDestCountryId(), 
            'values'     => $this->getDestCountryValues(), 
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('dest_region_id', 'select', array(
            'name'       => 'dest_region_id', 
            'label'      => $helper->__('Region/State'), 
            'title'      => $helper->__('Region/State'), 
            'required'   => false, 
            'value'		 => $model->getDestRegionId(), 
            'values'     => $this->getDestRegionValues(), 
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('dest_city', 'text', array(
            'name'       => 'dest_city', 
            'label'      => $helper->__('City'), 
            'title'      => $helper->__('City'), 
            'note'       => $helper->__('\'*\' or blank - matches any'), 
            'required'   => false, 
            'value'		 => $this->getCityValue(), 
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('dest_zip', 'text', array(
            'name'       => 'dest_zip', 
            'label'      => $helper->__('Zip/Postal Code From'), 
            'title'      => $helper->__('Zip/Postal Code From'), 
            'note'       => $helper->__('\'*\' or blank - matches any'), 
            'required'   => false, 
            'value'		 => $this->getDestZipValue(), 
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('dest_zip_to', 'text', array(
            'name'       => 'dest_zip_to', 
            'label'      => $helper->__('Zip/Postal Code To'), 
            'title'      => $helper->__('Zip/Postal Code To'), 
            'note'       => $helper->__('\'*\' or blank - matches any'), 
            'required'   => false, 
            'value'		 => $this->getDestZipToValue(), 
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('condition_name', 'select', array(
            'name'       => 'condition_name', 
            'label'      => $helper->__('Condition Name'), 
            'title'      => $helper->__('Condition Name'), 
            'required'   => true, 
            'value'		 => $model->getConditionName(), 
            'values'     => $this->getConditionNameValues(), 
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('condition_from_value', 'text', array(
            'name'       => 'condition_from_value', 
            'label'      => $helper->__('Condition Value From'), 
            'title'      => $helper->__('Condition Value From'), 
            'required'   => true, 
            'value'		 => floatval($model->getConditionFromValue()), 
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('condition_to_value', 'text', array(
            'name'       => 'condition_to_value', 
            'label'      => $helper->__('Condition Value To'), 
            'title'      => $helper->__('Condition Value To'), 
            'required'   => true, 
            'value'		 => floatval($model->getConditionToValue()), 
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('price', 'text', array(
            'name'       => 'price', 
            'label'      => $helper->__('Price'), 
            'title'      => $helper->__('Price'), 
            'required'   => true, 
            'value'		 => floatval($model->getPrice()), 
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('cost', 'text', array(
            'name'       => 'cost', 
            'label'      => $helper->__('Cost'), 
            'title'      => $helper->__('Cost'), 
            'required'   => true, 
            'value'		 => floatval($model->getCost()),
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('code', 'select', array(
            'name'       => 'method_code', 
            'label'      => $helper->__('Shipping Type'), 
            'title'      => $helper->__('Shipping Type'), 
            'required'   => false, 
            'value'		 => $model->getMethodCode(),
            'options'	 => Mage::helper('gls/rates')->getMethodCodeOptions(),
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('name', 'text', array(
            'name'       => 'method_name', 
            'label'      => $helper->__('Name'), 
            'title'      => $helper->__('Name'), 
            'required'   => false, 
            'value'		 => $model->getMethodName(),
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('description', 'text', array(
            'name'       => 'method_description', 
            'label'      => $helper->__('Description'), 
            'title'      => $helper->__('Description'), 
            'required'   => false, 
            'value'		 => $model->getMethodDescription(), 
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('sort_order', 'text', array(
            'name'       => 'sort_order', 
            'label'      => $helper->__('Sort order'), 
            'title'      => $helper->__('Sort order'), 
            'required'   => false, 
            'value'		 => $model->getSortOrder(), 
            'disabled'   => $isElementDisabled, 
        ));
        $fieldset->addField('external_id', 'text', array(
            'name'       => 'external_id', 
            'label'      => $helper->__('External ID'), 
            'title'      => $helper->__('External ID'), 
            'required'   => false, 
            'value'		 => $model->getExternalId(), 
            'disabled'   => $isElementDisabled, 
            'note'	 	 => $helper->__('Can be uses like "Consignor ID"'),
        ));

        Mage::dispatchEvent('gls_adminhtml_rate_edit_prepare_form', array('form' => $form));
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}