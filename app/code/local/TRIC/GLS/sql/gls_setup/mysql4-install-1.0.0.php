<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */


$installer = $this;
$installer->startSetup();

$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('shipping_gls')};
	CREATE TABLE {$installer->getTable('shipping_gls')} (
		`pk` int(10) unsigned NOT NULL auto_increment,
		`website_id` int(11) NOT NULL default '0',
		`dest_country_id` varchar(4) NOT NULL default '0',
		`dest_region_id` int(10) NOT NULL default '0',
		`dest_zip` varchar(10) NOT NULL default '',
		`condition_name` varchar(20) NOT NULL default '',
		`condition_value_min` decimal(12,4) NOT NULL default '0.0000',
		`condition_value_max` decimal(12,4) NOT NULL default '0.0000',
		`condition_type` varchar(16) NOT NULL default 'value',
		`method_code` varchar(64) NOT NULL,
		`method_name` varchar(64) NOT NULL,
		`method_description` varchar(255) NOT NULL,
		`price` decimal(12,4) NOT NULL default '0.0000',
		`cost` decimal(12,4) NOT NULL default '0.0000',
		PRIMARY KEY (`pk`),
		UNIQUE KEY `dest_country` (`website_id`,`dest_country_id`,`dest_region_id`,`dest_zip`,`condition_name`,`condition_value_min`,`condition_value_max`,`method_code`)
	) DEFAULT CHARSET=utf8;
");


try {
	$installer->run("ALTER TABLE `{$installer->getTable('sales_flat_quote')}` ADD `gls_pakkeshop` varchar(255);");
} catch(Exception $e) {}
try {
	$installer->run("ALTER TABLE `{$installer->getTable('sales_flat_quote')}` ADD `gls_afhenter` varchar(255);");
} catch(Exception $e) {}

$installer->endSetup();