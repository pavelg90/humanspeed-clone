<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_GLS_Model_Observer
{
	public function appendDroppointHtml($observer)
	{
		$block = $observer->getEvent()->getBlock();
		if($block instanceof Mage_Checkout_Block_Onepage_Shipping_Method_Available || $block instanceof AW_Onestepcheckout_Block_Onestep_Form_Shippingmethod)
		{
			$transport = $observer->getEvent()->getTransport();
			$html = $transport->getHtml();
			if(strpos($html, 'gls_bestway') !== false) {
				$html .= $block->getLayout()->createBlock('gls/droppoints')->setTemplate('gls/droppoints.phtml')->toHtml();
			}
			$transport->setHtml($html);
		}
	}
	
	public function saveData($event) 
    {
    	$prefix = "gls-";
    	$methodcode = 'gls';
		$request = Mage::app()->getRequest();
		$quote  = $event->getQuote();
		
		$shippingMethod = $quote->getShippingAddress()->getData('shipping_method');

		if(strpos($shippingMethod,$methodcode) !== false) 
		{
			$droppointsIds = Mage::helper('gls/droppoints')->getDroppointsIds();
			$shippingMethod = explode("_",$shippingMethod);
			if(in_array($shippingMethod[count($shippingMethod)-1],$droppointsIds)) 
			{
				$pakkeshopId = $request->getPost($prefix.'droppoint-id');
				
				if($pakkeshopId != '') 
				{
					$pakkeshop = Mage::helper('gls/pakkeshop')->getPakkeshop($pakkeshopId);
					try{
						
						if(!trim($pakkeshop->Streetname2)) {
							$pakkeshop->Streetname2 = "Pakkeshop: $pakkeshopId";
						}
						
						if(Mage::getStoreConfig('gls/general/new_data_save_version'))
						{
							$name = explode(' ',trim($request->getPost($prefix.'afhenter')),2);
							if(sizeof($name) == 1) {
								$firstname = $name[0];
								$lastname = '-';
							}
							else {
								$firstname = $name[0];
								$lastname = $name[1];
							}
			
							$street = trim($pakkeshop->Streetname)."\n".trim($pakkeshop->Streetname2);
							$postcode = trim($pakkeshop->ZipCode);
							$city = trim($pakkeshop->CityName);
							$telephone = trim($request->getPost($prefix.'telephone'));
							$fax = '';					
							$company = $pakkeshop->CompanyName;
						} 
						else 
						{
							$name = explode(' ',trim($pakkeshop->CompanyName),2);
							if(sizeof($name) == 1) {
								$firstname = $name[0];
								$lastname = '-';
							}
							else {
								$firstname = $name[0];
								$lastname = $name[1];
							}
			
							$street = trim($pakkeshop->Streetname)."\n".trim($pakkeshop->Streetname2);
							$postcode = trim($pakkeshop->ZipCode);
							$city = trim($pakkeshop->CityName);
							$telephone = trim($request->getPost($prefix.'telephone'));
							$fax = Mage::helper('gls')->__("Afhentes af:") . " " . $request->getPost($prefix.'afhenter');					
							$company = '';
						}
						
						$quote_shipping_address = $quote->getShippingAddress()
												->setFirstname($firstname)
												->setLastname($lastname)
												->setCompany($company)
												->setStreet($street)
												->setPostcode($postcode)
												->setCity($city)
												->setTelephone($telephone)
												->setFax($fax)
												->setDroppoint($pakkeshopId);
					
						
							$write = Mage::getSingleton('core/resource')->getConnection('core_write');
							$table = Mage::getSingleton('core/resource')->getTableName('sales_flat_quote_address');
							$write->query("UPDATE $table SET firstname='$firstname',lastname='$lastname',company='$company',street='$street',postcode='$postcode',city='$city',telephone='$telephone',fax='$fax',droppoint='$pakkeshopId' WHERE quote_id='".$quote->getId()."' AND address_type='shipping'");
							$quote->setShippingAddress($quote_shipping_address);
							$quote->save();
							unset($_SESSION['gls-afhenter']);
						
					}
					catch(Exception $e) {
						Mage::log($e->getMessage());
					}
				} else {
					try {

						$write = Mage::getSingleton('core/resource')->getConnection('core_write');
						$table = Mage::getSingleton('core/resource')->getTableName('sales_flat_quote_address');
						$write->query("UPDATE $table SET droppoint=0 WHERE quote_id='".$quote->getId()."' AND address_type='shipping'");
						
						$quote_shipping_address = $quote->getShippingAddress()->setDroppoint(0);
						$quote->setShippingAddress($quote_shipping_address);
						$quote->save();

					}
					catch(Exception $e) {
						Mage::log($e->getMessage());
					}
				}
			}
			else {
				try {

					$write = Mage::getSingleton('core/resource')->getConnection('core_write');
					$table = Mage::getSingleton('core/resource')->getTableName('sales_flat_quote_address');
					$write->query("UPDATE $table SET droppoint=0 WHERE quote_id='".$quote->getId()."' AND address_type='shipping'");
					
					$quote_shipping_address = $quote->getShippingAddress()->setDroppoint(0);
					$quote->setShippingAddress($quote_shipping_address);
					$quote->save();
						
				}
				catch(Exception $e) {
					Mage::log($e->getMessage());
				}
			}
			
		}
	}


	public function attachInfo($observer)
	{
		$quote = $observer->getEvent()->getQuote();

		$shippingMethod = explode('_',$quote->getShippingAddress()->getData('shipping_method'));
		$shippingMethod = $shippingMethod[0].'_'.$shippingMethod[1];

		if($shippingMethod == Mage::getStoreConfig('gls_pakkeshop/general/pakkeshop_shipping_method',$quote->getStoreId()) && $quote->getGlsPakkeshop() != '')
		{
			try{
				$pakkeshop = Mage::helper('gls_pakkeshop/pakkeshop')->getPakkeshopHtmlToQuote($quote);
				$quote->getShippingAddress()->setShippingDescription($quote->getShippingAddress()->getShippingDescription().$pakkeshop);
			}
			catch(Exception $e) {
				Mage::log($e->getMessage());
			}
		}
	}
	
	public function exportWhenShipmentIsCreated(Varien_Event_Observer $observer){
		
		$controllerName = Mage::app()->getRequest()->getControllerName();
		$actionName = Mage::app()->getRequest()->getActionName();
		
		$preventShipmentActions = array('addTrack','addComment','email');
		
		if(!$controllerName && !$actionName){
			return false;
		}
		elseif($controllerName == 'sales_order_shipment' && in_array($actionName,$preventShipmentActions)){
			return false;
		}
		
		$shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();
        $storeId = $order->getStoreId();
        if(Mage::getStoreConfig('gls/general/gls_csv_shipment',$storeId)){
	        $this->exportToGLSFormat($order);
        }
	}
	
	public function exportWhenOrderStatusChanged(Varien_Event_Observer $observer){
		
		$controllerName = Mage::app()->getRequest()->getControllerName();
		$actionName = Mage::app()->getRequest()->getActionName();
		
		$preventOrderActions = array('addComment');
		$preventShipmentActions = array('addTrack','addComment','email');
		$preventInvoiceActions = array('cancel');
		
		if($controllerName == 'sales_order_creditmemo'){
			return false;
		}
		elseif($controllerName == 'sales_order_invoice' && in_array($actionName,$preventInvoiceActions)){
			return false;
		}
		elseif($controllerName == 'sales_order_shipment' && in_array($actionName,$preventShipmentActions)){
			return false;
		}
		elseif($controllerName == 'sales_order' && in_array($actionName,$preventOrderActions)){
			return false;
		}
		
		// MageWorx_OrdersPro
		if($controllerName == 'orderspro_order_creditmemo'){
			return false;
		}
		elseif($controllerName == 'orderspro_order_invoice' && in_array($actionName,$preventInvoiceActions)){
			return false;
		}
		elseif($controllerName == 'orderspro_order_shipment' && in_array($actionName,$preventShipmentActions)){
			return false;
		}
		elseif($controllerName == 'orderspro_order' && in_array($actionName,$preventOrderActions)){
			return false;
		}
		
		$order = $observer->getEvent()->getOrder();
		$status = $order->getStatus();
		$storeId = $order->getStoreId();
		
		if(!Mage::getStoreConfig('gls/general/gls_csv_shipment',$storeId) && $status && $status == Mage::getStoreConfig('gls/general/gls_csv',$storeId)){
			$this->exportToGLSFormat($order);
		}
	}

	public function exportToGLSFormat($order,$export_filename = false)
	{
		$shippingMethod = explode('_',$order->getData('shipping_method'));

		if(sizeof($shippingMethod) > 2) {
			$glsIds = Mage::helper('gls')->getGLSMethodIds();
			$glsMethodIds = array();
			foreach ($glsIds as $id) {
				array_push($glsMethodIds,$id['pk']);
			}

			$glsIds = Mage::helper('gls')->getGLSMethodIds('pakkeshop');
			$glsPakkeshopIds = array();
			foreach($glsIds as $id) {
				array_push($glsPakkeshopIds,$id['pk']);
			}

			if($shippingMethod[0] == 'gls' && in_array($shippingMethod[2],$glsMethodIds))
			{
				$orderId = $order->getRealOrderId();
				try{

					$orderItems = $order->getItemsCollection();
					$weight = 0;

					foreach ($orderItems as $item) {
						if(!$item->isDummy()) {
							$weight += ($item->getWeight())*((int)$item->getQtyOrdered());
						}
					}

					if($weight == 0) { $weight = 1; }

					$pathname = Mage::getBaseDir()."/var/GLS";
					$mode = 0777;


					if($this->mkdir_recursive($pathname,$mode)) {
						$number_off_labels = 1;
						if(Mage::getStoreConfig('gls/general/gls_label_per_vare',$order->getStoreId())) {
							$number_off_labels = $order->getTotalQtyOrdered();
						}
						$i = 1;
						while($i <= $number_off_labels) 
						{
							if($export_filename) {
								$file =	$export_filename;
							} else {
								$filename = (Mage::getStoreConfig('gls/general/gls_label_per_vare',$order->getStoreId())) ? $orderId."_".$i : $orderId; 
								$file = $filename.".csv";
							}
							$fp = fopen($pathname."/".$file, 'a') or die("can't open file");
							fwrite($fp, "\"".$orderId."\",");
	
							$shipping_address = $order->getShippingAddress();
							
							if(Mage::getStoreConfig('gls/general/new_data_save_version'))
							{
								$company = ($shipping_address->getData('company')) ? $shipping_address->getData('company') : '';
								$street = $shipping_address->getStreet();
								$street1 = $street[0];
								if(sizeof($street) > 1) {
									$street2 = $street[1];
								} else {
									$street2 = '';
								}
								$region = $shipping_address->getRegion();
								$postcode = $shipping_address->getPostcode();
								$city = $shipping_address->getCity();
								$countryCode = $this->getGlsContryCode($shipping_address->getCountry());
								$afhenter = $shipping_address->getFirstname().' '.$shipping_address->getLastname();
								$email = $order->getCustomerEmail();
								$telephone = trim($shipping_address->getData("telephone"));
								
								$name = ($company) ? $company : $shipping_address->getFirstname().' '.$shipping_address->getLastname();
								$att = '';
								if($company) {
									$att = $shipping_address->getFirstname().' '.$shipping_address->getLastname();		
								}

							} 
							else
							{
								if($shipping_address->getLastname() == '-') {
									$company = $shipping_address->getFirstname();
								}
								else {
									$company = $shipping_address->getFirstname().' '.$shipping_address->getLastname();
								}
								$street = $shipping_address->getStreet();
								$street1 = $street[0];
								if(sizeof($street) > 1) {
									$street2 = $street[1];
								}
								else {
									$street2 = '';
								}
		
								$region = $shipping_address->getRegion();
								$postcode = $shipping_address->getPostcode();
								$city = $shipping_address->getCity();
								$countryCode = $this->getGlsContryCode($shipping_address->getCountry());
								
								$afhenter = explode(': ',$shipping_address->getFax());
								if(sizeof($afhenter) > 1) {
									$afhenter = $afhenter[1];
								}
								else {
									$afhenter = $order->getCustomerName();
								}
								$email = $order->getCustomerEmail();
								$telephone = trim($shipping_address->getData("telephone"));
								
								if($shipping_address->getData('company') != '') {
									$name = $shipping_address->getData("company");
								}
								else {
									$name = $shipping_address->getFirstname().' '.$shipping_address->getLastname();
								}
								
								if($shipping_address->getData('company') != '') {
									$att = $shipping_address->getFirstname().' '.$shipping_address->getLastname();
								}
								else {
									$att = '';
								}

								
							}
	
							if(in_array($shippingMethod[2],$glsPakkeshopIds)) 
							{
								fwrite($fp, "\"".utf8_decode($company)."\",");														
								fwrite($fp, "\"".utf8_decode($street1)."\",");
								fwrite($fp, "\"".utf8_decode($street2)."\",");
								fwrite($fp, "\"".utf8_decode($postcode)."\",");
								fwrite($fp, "\"".utf8_decode($city)."\",");
								fwrite($fp, "\"".utf8_decode($countryCode)."\",");
								fwrite($fp, "\"".utf8_decode(date("d-m-y"))."\",");
								fwrite($fp, "\"".utf8_decode(trim(str_replace('.',',',$weight)))."\",");
								fwrite($fp, "\"1\",");
								fwrite($fp, "\"\",");
								fwrite($fp, "\"\",");
								fwrite($fp, "\"A\",");
								fwrite($fp, "\"Z\",");
								fwrite($fp, "\"".utf8_decode(trim($afhenter))."\",");
								fwrite($fp, "\"\",");
								fwrite($fp, "\"".utf8_decode(Mage::getStoreConfig('gls/general/gls_afsendernr',$order->getStoreId()))."\",");
								fwrite($fp, "\"".utf8_decode($email)."\",");
								fwrite($fp, "\"".utf8_decode($telephone)."\",");
								
								$mail_notification = ""; // E
								$shop_return_service = ""; // B
								if(Mage::getStoreConfig('gls/general/gls_mail_notification',$order->getStoreId())) {
									$mail_notification = "E";
								}
								if(Mage::getStoreConfig('gls/general/gls_shop_return_service',$order->getStoreId())) {
									$shop_return_service = "B";
								}
								$services = $mail_notification.$shop_return_service;
								fwrite($fp, "\"".$services."\",");
								fwrite($fp, "\"".$shipping_address->getDroppoint()."\""); // FIX så pakkeshop kommer her :-)
								
								fwrite($fp, PHP_EOL);
								fclose($fp);
							}
							else 
							{
							
								$shipmentType = 'A';
								if($countryCode != '8'){
									$shipmentType = 'U';
								}
								
								$glsIds = Mage::helper('gls')->getGLSMethodIds('express10');
								$glsExpress10Ids = array();
								foreach($glsIds as $id) {
									array_push($glsExpress10Ids,$id['pk']);
								}
								
								$glsIds = Mage::helper('gls')->getGLSMethodIds('express12');
								$glsExpress12Ids = array();
								foreach($glsIds as $id) {
									array_push($glsExpress12Ids,$id['pk']);
								}
								
								if(in_array($shippingMethod[2],$glsExpress10Ids)){
									$shipmentType = '4';
								}
								elseif(in_array($shippingMethod[2],$glsExpress12Ids)){
									$shipmentType = '5';
								}
								
								fwrite($fp, "\"".utf8_decode($name)."\",");
								fwrite($fp, "\"".utf8_decode(trim($street1.' '.$street2))."\",");
								fwrite($fp, "\"".utf8_decode($region)."\",");
								fwrite($fp, "\"".utf8_decode($postcode)."\",");
								fwrite($fp, "\"".utf8_decode($city)."\",");
								fwrite($fp, "\"".utf8_decode($countryCode)."\",");
								fwrite($fp, "\"".utf8_decode(date("d-m-y"))."\",");
								fwrite($fp, "\"".utf8_decode(trim(str_replace('.',',',$weight)))."\",");
								fwrite($fp, "\"1\",");
								fwrite($fp, "\"\",");
								fwrite($fp, "\"\",");
								fwrite($fp, "\"A\",");
								fwrite($fp, "\"".utf8_decode($shipmentType)."\",");
								fwrite($fp, "\"".utf8_decode($att)."\",");
								$glsPrivatKommentar = utf8_decode($order->getData('gls_privat_kommentar'));
	
								if($glsPrivatKommentar && $glsPrivatKommentar != '') {
									fwrite($fp, "\"".$glsPrivatKommentar."\",");
								}
								else {
									fwrite($fp, "\"\",");
								}
								fwrite($fp, "\"".utf8_decode(Mage::getStoreConfig('gls/general/gls_afsendernr',$order->getStoreId()))."\",");
								fwrite($fp, "\"".utf8_decode($email)."\",");
								fwrite($fp, "\"".utf8_decode($telephone)."\",");
								
								$mail_notification = ""; // E
								$shop_return_service = ""; // B
								if(Mage::getStoreConfig('gls/general/gls_mail_notification',$order->getStoreId())) {
									$mail_notification = "E";
								}
								if(Mage::getStoreConfig('gls/general/gls_shop_return_service',$order->getStoreId())) {
									$shop_return_service = "B";
								}
								$services = $mail_notification.$shop_return_service;
								fwrite($fp, "\"".$services."\"");
								
								fwrite($fp, PHP_EOL);
								fclose($fp);
							}
							Mage::getSingleton('adminhtml/session')->addSuccess('GLS-fil er oprettet for ordre '.$orderId.'. Find den via FTP her: var/GLS/'.$file);

							$i++;
						}
					}
					else {
						Mage::getSingleton('adminhtml/session')->addError('GLS-fil kan ikke oprettes, da der opstod problemer med at oprette mappen "var/GLS". Kontakt din hosting-udbyder om dette.');
					}
				}
				catch(Exception $e) {
					Mage::log($e->getMessage());
					Mage::getSingleton('adminhtml/session')->addError('Der er opstået en fejl ved oprettelse af GLS-fil vedr. ordre '.$orderId.'.');
				}
			}
		}
	}

	private function getGlsContryCode($code) {
		$globalCountryCodes = array('AL'=>'70', 'DZ'=>'208', 'AD'=>'43', 'AO'=>'330', 'AI'=>'446', 'AG'=>'459', 'AR'=>'528', 'AM'=>'77', 'AW'=>'474', 'AU'=>'800', 'AT'=>'38', 'AZ'=>'78', 'BS'=>'453', 'BH'=>'640', 'BD'=>'666', 'BB'=>'469', 'BY'=>'73', 'BE'=>'2', 'BZ'=>'421', 'BJ'=>'284', 'BM'=>'413', 'BT'=>'675', 'BO'=>'516', 'BA'=>'93', 'BW'=>'391', 'BR'=>'508', 'BN'=>'703', 'BG'=>'100', 'BF'=>'236', 'BI'=>'328', 'KH'=>'696', 'CM'=>'302', 'CA'=>'404', 'CV'=>'247', 'KY'=>'463', 'CF'=>'306', 'TD'=>'244', 'CL'=>'512', 'CN'=>'720', 'CO'=>'480', 'CG'=>'318', 'CD'=>'322', 'CI'=>'272', 'HR'=>'92', 'CY'=>'196', 'CZ'=>'61', 'DK'=>'8', 'DJ'=>'338', 'DM'=>'460', 'DO'=>'456', 'EC'=>'500', 'EG'=>'220', 'GQ'=>'310', 'ER'=>'336', 'EE'=>'233', 'ET'=>'334', 'FO'=>'234', 'FJ'=>'815', 'FI'=>'32', 'FR'=>'1', 'PF'=>'822', 'GA'=>'314', 'GM'=>'252', 'GE'=>'76', 'DE'=>'4', 'GH'=>'276', 'GI'=>'292', 'GR'=>'9', 'GL'=>'304', 'GD'=>'473', 'GT'=>'416', 'GN'=>'260', 'GW'=>'257', 'GY'=>'488', 'HT'=>'452', 'HN'=>'424', 'HK'=>'740', 'HU'=>'64', 'IS'=>'352', 'IN'=>'664', 'ID'=>'700', 'IR'=>'616', 'IQ'=>'612', 'IE'=>'7', 'IL'=>'624', 'IT'=>'5', 'JM'=>'464', 'JP'=>'732', 'JO'=>'628', 'KZ'=>'79', 'KE'=>'346', 'KW'=>'636', 'KG'=>'83', 'LA'=>'684', 'LV'=>'428', 'LB'=>'604', 'LS'=>'395', 'LR'=>'268', 'LY'=>'216', 'LT'=>'440', 'LU'=>'19', 'MO'=>'743', 'MK'=>'96', 'MG'=>'370', 'MW'=>'386', 'MY'=>'701', 'MV'=>'667', 'ML'=>'232', 'MT'=>'46', 'MA'=>'204', 'MH'=>'824', 'MR'=>'228', 'MU'=>'373', 'MX'=>'412', 'FM'=>'823', 'MD'=>'74', 'MC'=>'492', 'MN'=>'716', 'MS'=>'470', 'MZ'=>'366', 'MM'=>'676', 'NA'=>'389', 'NP'=>'672', 'AN'=>'478', 'NL'=>'3', 'NC'=>'809', 'NZ'=>'804', 'NI'=>'432', 'NE'=>'240', 'NG'=>'288', 'NO'=>'28', 'MP'=>'820', 'OM'=>'649', 'PK'=>'662', 'PW'=>'825', 'PA'=>'442', 'PG'=>'801', 'PY'=>'520', 'PH'=>'708', 'PL'=>'60', 'PT'=>'10', 'QA'=>'644', 'RE'=>'638', 'RO'=>'66', 'RU'=>'75', 'RW'=>'324', 'SM'=>'674', 'SA'=>'632', 'SN'=>'248', 'CS'=>'98', 'SC'=>'355', 'SL'=>'264', 'SG'=>'706', 'SK'=>'63', 'SI'=>'91', 'SO'=>'342', 'ZA'=>'388', 'ES'=>'11', 'LK'=>'669', 'KN'=>'449', 'LC'=>'465', 'VC'=>'467', 'SD'=>'224', 'SZ'=>'393', 'SE'=>'30', 'CH'=>'39', 'SY'=>'608', 'TW'=>'736', 'TJ'=>'82', 'TH'=>'680', 'TG'=>'280', 'TT'=>'472', 'TN'=>'212', 'TR'=>'52', 'TM'=>'80', 'UG'=>'350', 'UA'=>'72', 'AE'=>'647', 'GB'=>'6', 'US'=>'400', 'UY'=>'524', 'UZ'=>'81', 'VU'=>'816', 'VA'=>'45', 'VE'=>'484', 'VN'=>'690', 'VG'=>'468', 'VI'=>'457', 'WF'=>'811', 'YE'=>'653', 'ZM'=>'378', 'ZW'=>'382');
		return $globalCountryCodes[$code];
	}

	public function importTrackingCode()
	{
		$pathname = Mage::getBaseDir()."/var/GLS-STATUS";
		$mode = 0777;

		if($this->mkdir_recursive($pathname,$mode))
		{
			if($files = Mage::helper('gls')->glob_ext_i(array("txt"),$pathname))
			{
				foreach($files as $file)
				{
					$file_content = file_get_contents($file);
					$data = explode(" ",$file_content);
					
					$order_num = null;
					
					$trackandtrace = trim($data[0]);
					if(isset($data[count($data)-1])){
						$order_num = trim($data[count($data)-1]);
					}

					if($order_num && $order = Mage::getModel('sales/order')->loadByIncrementId($order_num)){
						
						$shipment_collection = Mage::getResourceModel('sales/order_shipment_collection');
						$shipment_collection->addAttributeToFilter('order_id', $order->getId());

						foreach($shipment_collection as $shipment)
						{
							Mage::getModel('sales/order_shipment_api')->addTrack($shipment->getIncrementId(), 'custom', "GLS Tracking", $trackandtrace);
						}

						$notify = true;

						$besked = Mage::getStoreConfig('gls/trackandtrace/email_message',$order->getStoreId());
						$besked = str_replace("{{trackandtrace}}",$trackandtrace,$besked);
						
						if(Mage::getStoreConfig('gls/general/gls_csv_shipment',$order->getStoreId())){
							$order->addStatusToHistory($order->getStatus(), 'GLS Track & Trace: '.$trackandtrace, $notify);
							$order->save();
						}

						$order->sendOrderUpdateEmail($notify,$besked);
					}

					unlink($file);
				}
			}
		}
	}

	public function addMassAction($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction && strpos($block->getRequest()->getControllerName(),'sales_order') !== false)
        {
        	$block->addItem('gls_start', array(
					'label' => '-- -- --  '.Mage::helper('gls')->__('GLS').'   -- -- --',
					'url'   => '',
				)
			);
			
			$block->addItem('gls_auto', array(
					'label' => Mage::helper('gls')->__('Lav CSV filer'),
					'url'   => Mage::helper('adminhtml')->getUrl('adminhtml/adminhtml_gls_csv/createcsvfiles', array('_current'=>true)),
				)
			);
			
			$block->addItem('gls_end', array(
					'label' => Mage::helper('gls')->__('-- -- -- -- -- -- -- --'),
					'url'   => '',
				)
			);
		}
	}


	private function mkdir_recursive($pathname, $mode)
	{
		$file = new Varien_Io_File();
		$file->checkAndCreateFolder($pathname,$mode);
		return true;
	}


	public function handleSalesRulesActionForm($observer)
	{
		$form = $observer->getForm();
		$actionsSelect = $form->getElement('simple_action');
        if ($actionsSelect){
            $actionsSelect->setValues(array_merge(
                $actionsSelect->getValues(), 
                array(TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES => Mage::helper('gls')->__('Fri fragt på specifikke GLS fragtsatser'))
            ));
        }        

		$values = array();
		foreach(Mage::getModel('gls/carrier_gls')->getAllowedMethods() as $code => $label) {
			$values[] = array('label' => $label,'value' => $code);
		}
 
        $fieldset = $form->getElement('action_fieldset');

		$fieldset->addField(TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES_FIELD, 'multiselect', array(
	        'title'     => Mage::helper('gls')->__('GLS Fragtsatser'),
            'label'     => Mage::helper('gls')->__('GLS Fragtsatser'),
            'name'      => TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES_FIELD,
            'values'    => $values,
            'note'		=> Mage::helper('gls')->__('Et beløb på 0, vil udløse fri fragt, mens et eventuelt beløb vil blive fratrukket fragtprisen. Der udløses ikke negative fragtsatser'),
        ));
        
        return $this; 
	}
	
	public function handleGlsSalesRulesActionData($observer)
	{
		$request = $observer->getEvent()->getRequest();
		$values = $request->getParam(TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES_FIELD);
		if($values && is_array($values)) {
			$request->setPost(TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES_FIELD,implode(',',$values));	
		} else {
			$request->setPost(TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES_FIELD,"");	
		}
	}
	
	public function specificFreeshipping($observer)
	{
		$quote = $observer->getEvent()->getQuote();
		$quoteId = $quote->getId();
		
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('core_read');
		
		$appliedRuleIds = $quote->getAppliedRuleIds();

		if(!$appliedRuleIds && $quoteId){
			$table = $resource->getTableName('sales/quote');
			$query = "SELECT applied_rule_ids FROM $table WHERE entity_id = $quoteId";
			$appliedRuleIds = $read->fetchOne($query);
		}
		
		if($appliedRuleIds)
		{
			$field = TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES_FIELD;
			$simple_action = TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES;
			$table = $resource->getTableName('salesrule');
			$query = "SELECT $field,discount_amount FROM $table WHERE simple_action = '$simple_action' AND rule_id IN ($appliedRuleIds) ORDER BY sort_order ASC";

			$results = $read->fetchAll($query);

			if(count($results))
			{	
				$discount_amounts = array();
				$rate_codes = array();
				foreach($results as $salesRule)
				{
					if(isset($salesRule[$field]) && $salesRule[$field])
					{
						$rate_codes = array_merge($rate_codes,explode(",",$salesRule[$field]));
						
						foreach($rate_codes as $rate_code) {
							if(isset($salesRule['discount_amount']) && $salesRule['discount_amount'] > 0 && !isset($discount_amounts[$rate_code])) {
								$discount_amounts[$rate_code] = $salesRule['discount_amount'];
							}
						}
					}
				}
				$rate_codes = array_unique($rate_codes);

				$result = $observer->getEvent()->getResult();
				
				foreach($result->getAllRates() as $method) {
					if(in_array($method->getMethod(),$rate_codes)) {
						if(isset($discount_amounts[$method->getMethod()])) {
							$method->setPrice(max($method->getPrice()-$discount_amounts[$method->getMethod()],0));
						} else {
							$method->setPrice(0);
						}
					}
				}
			}
		}

	}
	
	public function getSalesRulesActionJsScript()
	{
		$simple_action = TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES;
		$field = TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES_FIELD;
		$script = '<script type="text/javascript">';
		$script .= "function gls_show_methods() {
				if ('$simple_action' == $('rule_simple_action').value) {
				    $('rule_$field').up().up().show();
				    $('rule_discount_amount').up().insert($('note_$field'));
				    $$('select#rule_simple_free_shipping option')[0].selected = true;
				} else {
					$('rule_$field').up().up().hide();
					$('rule_$field').up().insert($('note_$field'));
				}
			}
			gls_show_methods();
			$('rule_simple_action').observe('change',gls_show_methods);
		";	
		$script .= '</script>';
		
		return $script;
		
	}
	public function addSalesRulesActionFormJs($observer)
	{
		
		$block = $observer->getEvent()->getBlock();
		if($block instanceof Mage_Adminhtml_Block_Promo_Quote_Edit)
		{
			$simple_action = TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES;
			$field = TRIC_GLS_Model_Carrier_Gls::FREE_SHIPPING_RATES_FIELD;
			
			$script = $this->getSalesRulesActionJsScript();
			
			try{ 
				if($pacsoft = Mage::getModel('pacsoft/observer')) {
					if(method_exists($pacsoft,'getSalesRulesActionJsScript')) {
						$script .= $pacsoft->getSalesRulesActionJsScript(); 
					}
				}
			} catch(Exception $e) { }
			
			
			try{
				if($extrasalesrules = Mage::getModel('extrasalesrules/observer')) {
					if(method_exists($extrasalesrules,'addSalesRulesActionFormJs')) {
						$extrasalesrules->addSalesRulesActionFormJs($observer);	
					}
				} else {
					throw new Exception("Release Script");
				}
			} 
			catch(Exception $e) 
			{
				$extra_scripts = array();
				try{
					if($pacsoft = Mage::getModel('pacsoft/carrier_postdk')) {
						if(method_exists($pacsoft,'getExtraJsSalesRuleScript')) {
							$extra_scripts[] = $pacsoft->getExtraJsSalesRuleScript();
						}
					}
				} catch(Exception $e) {}
				try{
					if($gls = Mage::getModel('gls/carrier_gls')) {
						if(method_exists($gls,'getExtraJsSalesRuleScript')) {
							$extra_scripts[] = $gls->getExtraJsSalesRuleScript();
						}
						
					}
				} catch(Exception $e) {}							
				
				$script .= '<script type="text/javascript">';
				$script .= "function extra_sales_rules_form_handling() {
						$('rule_discount_amount').up().up().show();
						$('rule_discount_qty').up().up().show();
						$('rule_discount_step').up().up().show();
						$('rule_apply_to_shipping').up().up().show();
						$('rule_simple_free_shipping').up().up().show();
						$('rule_stop_rules_processing').up().up().show();
						";
						
				$script .= implode("\n",$extra_scripts);		
						
				$script .= "
					}
					extra_sales_rules_form_handling();
					$('rule_simple_action').observe('change',extra_sales_rules_form_handling);
				";	
				$script .= '</script>';
			}
			
			$transport = $observer->getEvent()->getTransport();
			$html = $transport->getHtml();
			$transport->setHtml($html.$script);
		}
	}
}