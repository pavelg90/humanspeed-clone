<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_GLS_Helper_Pakkeshop extends Mage_Core_Helper_Abstract
{
	public function getPakkeshop($id)
	{
		$content = false;
		$url = "http://gls.services.tric.dk/pakkeshop.php?ParcelShopNumber=$id";

		if(function_exists('curl_version')) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $url);
			$content = utf8_encode(curl_exec($ch));
			curl_close($ch);
		} else {
			$content = file_get_contents($url);
		}
		if($content) {
			$shop = json_decode($content);
			if($shop != NULL){
				return $shop;
			} else{
				return false;
			}
		}
		return false;
	}
	
	public function getPakkeshopDirect($id)
	{
		$client = new Zend_Soap_Client(Mage::getStoreConfig('gls/settings/webserviceurl',Mage::app()->getStore()->getId()), array('encoding' => 'UTF-8','soap_version' => SOAP_1_1));

		$shop = $client->GetOneParcelShop(array(
				'ParcelShopNumber' => $id
			))->GetOneParcelShopResult;
		if($shop != NULL){
			return $shop;
		} else{
			return false;
		}
	}

	public function getPakkeshopHtml($order)
	{
		$shippingMethod = explode('_',$order->getData('shipping_method'));
		$shippingMethod = $shippingMethod[0].'_'.$shippingMethod[1];

		if(!$order || !$order->getId() || $shippingMethod != Mage::getStoreConfig('gls/general/pakkeshop_shipping_method',$order->getStoreId())){
			return false;
		}
		if($order->getGlsPakkeshop()){
			$pakkeshop = $this->getPakkeshop($order->getGlsPakkeshop());
			$shopHtml = '<br/><br/>';
			$shopHtml .= '<strong>'.trim($pakkeshop->CompanyName).'</strong><br/>';
			$shopHtml .= trim($pakkeshop->Streetname).'<br/>';
			$shopHtml .= trim($pakkeshop->Streetname2).'<br/>';
			$shopHtml .= trim($pakkeshop->ZipCode).' '.trim($pakkeshop->CityName).'<br/>';
			$shopHtml .= 'Afhentes af: '.$order->getGlsAfhenter();
			return $shopHtml;
		}
		else{
			return false;
		}
	}

	public function getPakkeshopHtmlToQuote($quote)
	{
		$shippingMethod = explode('_',$quote->getShippingAddress()->getData('shipping_method'));
		$shippingMethod = $shippingMethod[0].'_'.$shippingMethod[1];

		if($shippingMethod != Mage::getStoreConfig('gls/general/pakkeshop_shipping_method',$quote->getStoreId())){
			return false;
		}

		if($quote->getGlsPakkeshop()){
			$pakkeshop = $this->getPakkeshop($quote->getGlsPakkeshop());
			$shopHtml = '<br/><br/>';
			$shopHtml .= '<strong>'.trim($pakkeshop->CompanyName).'</strong><br/>';
			$shopHtml .= trim($pakkeshop->Streetname).'<br/>';
			$shopHtml .= trim($pakkeshop->Streetname2).'<br/>';
			$shopHtml .= trim($pakkeshop->ZipCode).' '.trim($pakkeshop->CityName).'<br/>';
			$shopHtml .= 'Afhentes af: '.$quote->getGlsAfhenter();
			return $shopHtml;
		}
		else{
			return false;
		}
	}

	public function isOsc(){
		$storeId = Mage::app()->getStore()->getId();
		if(Mage::getStoreConfig('gls/general/gls_osc',$storeId) == 1){
			return true;
		}
		else{
			return false;
		}
	}
}