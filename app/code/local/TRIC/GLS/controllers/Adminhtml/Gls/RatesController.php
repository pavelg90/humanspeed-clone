<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Adminhtml_Gls_RatesController extends Mage_Adminhtml_Controller_Action
{
	protected function _isAllowed()
	{
		return Mage::getSingleton('admin/session')->isAllowed('gls/rates');
	}
	
	public function indexAction()
    {
        $helper = $this->__getHelper();
        $this->_title($helper->__('GLS Shipping Rates'));
        $this->_initAction();
        $this->renderLayout();
    }
    
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }
    
    public function editAction()
    {
        $helper = $this->__getHelper();
        $this->_title($helper->__('Shipping Rates'));
        $id = (int)$this->getRequest()->getParam('pk');
        $model = $this->getModel();
        if($id){
            $model->load($id);
            if(!$model->getId()){
                $this->_getSession()->addError($helper->__('This rate no longer exists.'));
                $this->__redirect('*/*/');
                return;
            }
        }
        $this->_title($model->getId() ? $model->getTitle() : $helper->__('New Shipping Rate'));
        $data = $this->_getSession()->getFormData(true);
        if(is_array($data) && !empty($data)){
        	$model->addData($data);
        }
        Mage::register('gls_rate', $model);
        $title = $id ? $helper->__('Edit Shipping Rate') : $helper->__('New Shipping Rate');
        $this->_initAction()->_addBreadcrumb($title, $title);
        $this->renderLayout();
    }
    
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            $helper = $this->__getHelper();
            $model = $this->getModel();
            $session = $this->_getSession();
            
            if($id = (int)$this->getRequest()->getParam('pk'))
            {
	            $model->load($id);
            }
            
            if(isset($data['dest_city']) && $data['dest_city'] == "*") {
	            $data['dest_city'] = "";
            }
            if(isset($data['dest_zip']) && $data['dest_zip'] == "*") {
	            $data['dest_zip'] = "";
            }
            if(isset($data['dest_zip_to']) && $data['dest_zip_to'] == "*") {
	            $data['dest_zip_to'] = "";
            }
            
            if(isset($data['addons']))
            {
            	$data['addons'] = array_unique($data['addons']);
	            $data['addons'] = implode(",",$data['addons']);
            }
			
			if(isset($data['dest_country_id']) && is_array($data['dest_country_id'])){
				$data['dest_country_id'] = ",".implode(",",$data['dest_country_id']).",";
			}
			if(isset($data['dest_country_id']) && strpos($data['dest_country_id'],"*") !== false) {
	            $data['dest_country_id'] = 0;
            }

            $model->addData($data);
            Mage::dispatchEvent('gls_rate_prepare_save', array('model' => $model, 'request' => $this->getRequest()));
            
            try {
                $model->save();
                $session->addSuccess($helper->__('The shipping rate has been saved.'));
                $session->setFormData(true);
                if ($this->getRequest()->getParam('back')) {
                    $this->__redirect('*/*/edit', array('pk' => $model->getId(), '_current' => true));
                    return;
                }
                $this->__redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $session->addError($e->getMessage());
            } catch (Exception $e) {
                $session->addException($e, $helper->__('An error occurred while saving the shipping rate.'));
            }
            
            $session->setFormData($data);
            $this->__redirect('*/*/edit', array('pk' => $this->getRequest()->getParam('pk')));
            return;
        }
        $this->__redirect('*/*/');
    }
    
    public function deleteAction()
    {
        $helper = $this->__getHelper();
        if ($id = (int) $this->getRequest()->getParam('pk')) {
            $title = '';
            try {
                $model = $this->getModel();
                $model->load($id);
                $title = $model->getTitle();
                $model->delete();
                $this->_getSession()->addSuccess($helper->__('The shipping rate has been deleted.'));
                Mage::dispatchEvent('gls_rate_on_delete', array('title' => $title, 'status' => 'success'));
                $this->__redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::dispatchEvent('gls_rate_on_delete', array('title' => $title, 'status' => 'fail'));
                $this->_getSession()->addError($e->getMessage());
                $this->__redirect('*/*/edit', array('pk' => $id));
                return;
            }
        }
        $this->_getSession()->addError($helper->__('Unable to find a shipping rate to delete.'));
        $this->__redirect('*/*/');
    }
    
    public function massDeleteAction()
    {
        $helper = $this->__getHelper();
        $rateIds = $this->getRequest()->getParam('pk');
        if (is_array($rateIds)) {
            if (!empty($rateIds)) {
                try {
                    foreach ($rateIds as $rateId) {
                        $model = $this->getModel();
                        $model->load($rateId);
                        $title = $model->getTitle();
                        $model->delete();
                        Mage::dispatchEvent('gls_rate_on_delete', array('title' => $title, 'status' => 'success'));
                    }
                    $this->_getSession()->addSuccess(
                        $helper->__('Total of %d record(s) have been deleted.', count($rateIds))
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        } else $this->_getSession()->addError($helper->__('Please select shipping rate(s).'));
        $this->__redirect('*/*/index');
    }
    
    public function exportAction()
	{
		if($this->getRequest()->getParam('shippingMethod') == 'carriers_gls'){
		
			$fileName   = Mage::helper('gls')->__('GLS-ShippingRates').'.csv';
	
			$gridBlock  = $this->getLayout()->createBlock('gls_adminhtml/rates_exportGrid');
			$website    = Mage::app()->getWebsite($this->getRequest()->getParam('website'));
			
			if ($this->getRequest()->getParam('conditionName')) {
				$conditionName = $this->getRequest()->getParam('conditionName');
			} else {
				$conditionName = $website->getConfig('carriers/gls/condition_name');
			}
			
			$gridBlock->setWebsiteId($website->getId())->setConditionName($conditionName);
			$content = $gridBlock->getCsvFile();
			
			$this->_prepareDownloadResponse($fileName, $content);
		
		}
		else{
			$fileName   = 'tablerates.csv';
			/** @var $gridBlock Mage_Adminhtml_Block_Shipping_Carrier_Tablerate_Grid */
			$gridBlock  = $this->getLayout()->createBlock('adminhtml/shipping_carrier_tablerate_grid');
			$website    = Mage::app()->getWebsite($this->getRequest()->getParam('website'));
			if ($this->getRequest()->getParam('conditionName')) {
				$conditionName = $this->getRequest()->getParam('conditionName');
			} else {
				$conditionName = $website->getConfig('carriers/tablerate/condition_name');
			}
			$gridBlock->setWebsiteId($website->getId())->setConditionName($conditionName);
			$content    = $gridBlock->getCsvFile();
			$this->_prepareDownloadResponse($fileName, $content);
		}
	}
		
	protected function getModel()
    {
        $model = Mage::getModel('gls/rate');
        $model->setWebsiteId($this->_getWebsiteId());
        return $model;
    }
	
	protected function __getHelper()
    {
        return Mage::helper('gls/rates');
    }
    
    protected function _getWebsiteId()
    {
        return $this->__getHelper()->getWebsiteId();
    }
	
	protected function _initAction()
    {
        $helper = $this->__getHelper();
        $this->loadLayout();
        return $this;
    }
	
	protected function __redirect($path, $arguments = array())
    {
        return $this->_redirect($path, array_merge(array('website' => $this->_getWebsiteId()), $arguments));
    }
	
}