<?php
class Wireframed_ProductSwatches_Helper_Data extends Mage_Core_Helper_Abstract {
	
	public function outputProductSwatches( $_product ) {
				
		//Get swatches options
		$swatches_settings	= new EM_Colorswatches_Block_Settings;
		$swatches_array		= $swatches_settings->get_option_swatches();
		$swatches_folder	= Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'swatches';
		
		//Load product attributes
		$AllowAttributes = $_product->getTypeInstance(true)->getConfigurableAttributes($_product);
		$skipSaleableCheck = Mage::helper('catalog/product')->getSkipSaleableCheck();
		$allProducts = $_product->getTypeInstance(true)->getUsedProducts(null, $_product);
		$option = array();
		
		
		//Create array with value->img
		foreach( $swatches_array as $key => $value ) {
			$swatches[$value['value']] = $value['img'];
		}
			
		
		foreach ($allProducts as $product ) {
			// only show available option
			if(!$product->isSaleable()) continue;
			foreach ($AllowAttributes as $attribute) {
				$productAttribute   = $attribute->getProductAttribute();
				$productAttributeId = $productAttribute->getId();
				$option_array[$productAttribute->getAttributeCode()][]=$productAttribute->getFrontend()->getValue($product);
			}
		}
		
		//Filter to only show each option once
		foreach( $option_array  as $key => $value ){
			foreach( array_unique( $value ) as $option ) {
				$options[] = $option;
			}
		}
		
		//Filter the two arrays, $swatches and $options, returning array with options[value]->swatches[value]
		$options = array_flip($options);		
		$final_options = array_intersect_key( $swatches, $options );
		
		//Output
		ob_start();
			
		foreach( $final_options as $key => $value ) { ?>
			<img class="swatch-img" src="<?php echo $swatches_folder . '/' . $value; ?>" alt="<?php echo $key; ?>">	
		<?php
		}
		
		$output = ob_get_clean();
		
		//var_dump( $option_array );
		
		return $output;
	}
}