<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

$installer = $this;
$installer->startSetup();

try {
	$table = $installer->getTable('shipping_gls');
	$installer->run("ALTER TABLE $table CHANGE  `external_id`  `external_id` VARCHAR( 32 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  ''");
} catch(Exception $e) {
	Mage::log($e->getMessage(),null,'gls.log',true);
}

$installer->endSetup();