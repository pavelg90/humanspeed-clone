<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */


class TRIC_GLS_Block_Adminhtml_Rates_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        $this->_objectId = 'pk';
        $this->_blockGroup = 'gls';
        $this->_controller = 'adminhtml_rates';
        parent::__construct();
        $helper = $this->_getHelper();

        $this->_updateButton('save', 'label', $helper->__('Save Shipping Rate'));
        $this->_updateButton('delete', 'label', $helper->__('Delete Shipping Rate'));
    }


    protected function _getHelper()
    {
        return Mage::helper('gls/rates');
    }

    protected function getWebsite()
    {
        if (is_null($this->_website))
        {
	        $this->_website = $this->_getHelper()->getWebsite();
        }
        return $this->_website;
    }

    public function getWebsiteId()
    {
        return $this->_getHelper()->getWebsiteId($this->getWebsite());
    }

    protected function getSession()
    {
        return Mage::getSingleton('admin/session');
    }

    protected function getModel()
    {
        return Mage::registry('gls_rate');
    }

    public function getHeaderText()
    {
        $helper = $this->_getHelper();
        $model = $this->getModel();
        if ($model && $model->getId()) {
            return $helper->__("Edit Rate '%s'", $this->htmlEscape($model->getTitle()));
        } else {
            return $helper->__('New Rate');
        }
    }

    protected function _prepareLayout()
    {
        $json = $this->helper('directory')->getRegionJson();
        $this->_formScripts[] = 'var updater = new RegionUpdater("gls_rate_dest_country_id", "none", "gls_rate_dest_region_id", '.$json.', "disable")';
        return parent::_prepareLayout();
    }

    protected function getURLParams()
    {
        return array('website' => $this->getWebsiteId());
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/*/', $this->getURLParams());
    }

    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete', array_merge(
            array($this->_objectId => $this->getRequest()->getParam($this->_objectId)), $this->getURLParams()
        ));
    }

    public function getFormActionUrl()
    {
        if ($this->hasFormActionUrl()) return $this->getData('form_action_url');
        return $this->getUrl('*/*/save', $this->getURLParams());
    }
}