<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */


$installer = $this;
$installer->startSetup();

$table = $installer->getTable('shipping_gls');

try {
	$installer->run("ALTER TABLE $table ADD dest_city varchar(64) NOT NULL default '' AFTER dest_region_id;");
} catch(Exception $e) {}
try {
	$installer->run("ALTER TABLE $table ADD dest_zip_to VARCHAR( 10 ) NOT NULL DEFAULT '' AFTER dest_zip;");
} catch(Exception $e) {}
try {
	$installer->run("ALTER TABLE $table CHANGE condition_value_min condition_from_value DECIMAL( 12, 4 ) NOT NULL DEFAULT  '0.0000';");
} catch(Exception $e) {}
try {
	$installer->run("ALTER TABLE $table CHANGE condition_value_max condition_to_value DECIMAL( 12, 4 ) NOT NULL DEFAULT  '0.0000';");
} catch(Exception $e) {}
try {
	$installer->run("ALTER TABLE $table ADD sort_order int(11) NOT NULL default '0';");
} catch(Exception $e) {}
try {
	$installer->run("ALTER TABLE $table ADD external_id varchar(32) NOT NULL default '';");
} catch(Exception $e) {}
try {
	$installer->run("ALTER TABLE $table ADD active TINYINT NOT NULL DEFAULT '1';");
} catch(Exception $e) {}

$installer->endSetup();