<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_GLS_Adminhtml_Gls_ConfigController extends Mage_Adminhtml_Controller_Action
{
	protected function _isAllowed()
	{
		return Mage::getSingleton('admin/session')->isAllowed('gls/config');
	}

	protected function _construct()
	{
		$this->setFlag('index', 'no-preDispatch', true);
		return parent::_construct();
	}

	public function exportAction()
	{
		if($this->getRequest()->getParam('shippingMethod') == 'carriers_gls'){

			$websiteModel = Mage::app()->getWebsite($this->getRequest()->getParam('website'));

			if ($this->getRequest()->getParam('conditionName'))
			{
				$conditionName = $this->getRequest()->getParam('conditionName');
			}
			else
			{
				$conditionName = $websiteModel->getConfig('carriers/gls/condition_name');
			}

			$tableratesCollection = Mage::getResourceModel('gls_shipping/carrier_gls_collection');
			$tableratesCollection->setConditionFilter($conditionName);
			$tableratesCollection->setWebsiteFilter($websiteModel->getId());
			$tableratesCollection->load();

			$csv = '';

			$conditionName = Mage::getModel('gls_shipping/carrier_gls')->getCode('condition_name_short', $conditionName);
			$conditionName = explode(' ',$conditionName);
			$conditionName = $conditionName[0];

			$csvHeader = array(
				'"'.Mage::helper('adminhtml')->__('Country').'"',
				'"'.Mage::helper('adminhtml')->__('Region/State').'"',
				'"'.Mage::helper('adminhtml')->__('Zip/Postal Code').'"',
				'"'.$conditionName.' '.Mage::helper('adminhtml')->__('from').'"',
				'"'.$conditionName.' '.Mage::helper('adminhtml')->__('to').'"',
				'"'.Mage::helper('adminhtml')->__('Shipping Price').'"',
				'"'.Mage::helper('adminhtml')->__('Method Code').'"',
				'"'.Mage::helper('adminhtml')->__('Method Name').'"',
				'"'.Mage::helper('adminhtml')->__('Method Description').'"',
				'"'.Mage::helper('adminhtml')->__('Condition Type').'"'
			);

			$csv .= implode(',', $csvHeader)."\n";

			foreach ($tableratesCollection->getItems() as $item)
			{
				if ($item->getData('dest_country') == '')
				{
					$country = '*';
				}
				else
				{
					$country = $item->getData('dest_country');
				}

				if ($item->getData('dest_region') == '')
				{
					$region = '*';
				}
				else
				{
					$region = $item->getData('dest_region');
				}

				if ($item->getData('dest_zip') == '')
				{
					$zip = '*';
				}
				else
				{
					$zip = $item->getData('dest_zip');
				}

				$csvData = array(
					$country,
					$region,
					$zip,
					$item->getData('condition_value_min'),
					$item->getData('condition_value_max'),
					$item->getData('price'),
					$item->getData('method_code'),
					$item->getData('method_name'),
					$item->getData('method_description'),
					$item->getData('condition_type')
				);

				foreach ($csvData as $cell)
				{
					$cell = '"'.str_replace('"', '""', $cell).'"';
				}

				$csv .= implode(',', $csvData)."\n";
			}

			header('Pragma: public');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

			header("Content-type: application/octet-stream");
			header("Content-disposition: attachment; filename=gls.csv");
			echo $csv;
			exit;
		}
		else{
			$fileName   = 'tablerates.csv';
			/** @var $gridBlock Mage_Adminhtml_Block_Shipping_Carrier_Tablerate_Grid */
			$gridBlock  = $this->getLayout()->createBlock('adminhtml/shipping_carrier_tablerate_grid');
			$website    = Mage::app()->getWebsite($this->getRequest()->getParam('website'));
			if ($this->getRequest()->getParam('conditionName')) {
				$conditionName = $this->getRequest()->getParam('conditionName');
			} else {
				$conditionName = $website->getConfig('carriers/tablerate/condition_name');
			}
			$gridBlock->setWebsiteId($website->getId())->setConditionName($conditionName);
			$content    = $gridBlock->getCsvFile();
			$this->_prepareDownloadResponse($fileName, $content);
		}

	}
}      