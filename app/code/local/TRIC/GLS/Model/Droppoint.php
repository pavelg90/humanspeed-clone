<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Model_Droppoint extends Mage_Core_Model_Abstract
{
	protected $_pathname = "";
	protected $_use_cache = false;
	protected $_cache_type = "";
	protected $_cache_ttl = 86400;
	protected $_prefix = 'gls_droppoints';
	
	public function __construct()
	{
		$this->_pathname = Mage::getBaseDir('var').DS.$this->_prefix;	
		$this->_use_cache = (Mage::helper('gls')->getGLSConfigDataDroppoints('use_cache',Mage::app()->getStore()->getId())) ? true : false;
		$this->_cache_type = (Mage::helper('gls')->getGLSConfigDataDroppoints('cache_type',Mage::app()->getStore()->getId())) ? Mage::helper('gls')->getGLSConfigDataDroppoints('cache_type',Mage::app()->getStore()->getId()) : false;
		$this->_cache_ttl = (Mage::helper('gls')->getGLSConfigDataDroppoints('cache_ttl',Mage::app()->getStore()->getId())) ? Mage::helper('gls')->getGLSConfigDataDroppoints('cache_ttl',Mage::app()->getStore()->getId()) : 24*60*60;	
	}
	
	public function getDroppointJson($postalcode = false,$countrycode = false)
	{
		$postalcode = str_replace(" ","",$postalcode);
		if(!$postalcode)
		{
			return json_encode(array());
		}
		if(!$countrycode)
		{
			return json_encode(array());
		}

		if($this->_use_cache && $json = $this->getLocalDroppointResponse($postalcode,$countrycode))
		{
			return $json;
		}
		else
		{
			return $this->getRemoteDroppointResponse($postalcode,$countrycode);
		}
	}
	
	private function getRemoteDroppointResponse($postalcode,$countrycode)
	{	
		$url = "http://gls.services.tric.dk/api.php?postalCode=$postalcode&countryCode=$countrycode";
		if(function_exists('curl_version')) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $url);
			$content = utf8_encode(curl_exec($ch));
			curl_close($ch);
		} else {
			$content = file_get_contents($url);
		}
		
		if($content) {
			if($this->_cache_type == 'files')
			{
				$filename = $this->_pathname."/$countrycode"."_"."$postalcode.json";
				file_put_contents($filename,$content);
			}
			elseif($this->_cache_type == 'cache')
			{
				$cachename = $this->_prefix."_".$countrycode."_".$postalcode;
				$cache = Mage::getSingleton('core/cache');
				$cache->save($content,$cachename, array(), $this->_cache_ttl);	
			}
		}
		
		return $content;
	}
	
	
	private function getLocalDroppointResponse($postalcode,$countrycode)
	{
		if($this->_use_cache)
		{
			if($this->_cache_type == 'files')
			{
				if($this->createDroppointResponseFolder())
				{
					$filename = $this->_pathname."/$countrycode"."_"."$postalcode.json";
					$timeToLive = $this->_cache_ttl;
					if(file_exists($filename) && (time() - filectime($filename) < $timeToLive))
					{
						return file_get_contents($filename);
					}
				}
			}
			elseif($this->_cache_type == 'cache')
			{
				$cachename = $this->_prefix."_".$countrycode."_".$postalcode;
				$cache = Mage::getSingleton('core/cache');
				
				if($json = $cache->load($cachename))
				{
					return $json;
				}
			}
		}
		return false;		
	}
	
	
	private function createDroppointResponseFolder()
	{
		$file = new Varien_Io_File();
		return $file->mkdir($this->_pathname);
	}
}