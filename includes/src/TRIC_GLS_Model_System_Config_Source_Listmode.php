<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_GLS_Model_System_Config_Source_Listmode
{
	public function toOptionArray()
	{
		$helper = Mage::helper('gls');
		$arr = array();
		$arr[] = array('value'=>'list', 'label'=>$helper->__("List"));
		$arr[] = array('value'=>'dropdown', 'label'=>$helper->__("Dropdown"));
		$arr[] = array('value'=>'map', 'label'=>$helper->__("Map with locations"));

		return $arr;
	}
}