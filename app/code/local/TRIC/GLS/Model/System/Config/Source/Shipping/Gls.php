<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_GLS_Model_System_Config_Source_Shipping_Gls
{
	public function toOptionArray()
	{
		$tableRate = Mage::getSingleton('gls_shipping/carrier_gls');
		$arr = array();

		foreach ($tableRate->getCode('condition_name') as $k=>$v)
		{
			$arr[] = array('value'=>$k, 'label'=>$v);
		}
		return $arr;
	}
}