<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2012 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Model_Adminhtml_System_Config_Source_Cachetypes
{
    public function toOptionArray()
    {
	    $array[] = array('value'=>'cache', 'label'=>Mage::helper('gls')->__('Magento Cache'));
	    $array[] = array('value'=>'files', 'label'=>Mage::helper('gls')->__('Filer'));
        return $array;
    }
}
