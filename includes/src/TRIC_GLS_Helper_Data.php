<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getGLSConfigDataDroppoints($value = '',$storeid = 0){
		return trim(Mage::getStoreConfig(sprintf('gls/droppoints/%s', $value),$storeid));
	}

	public function getShippingDescription($order){
		$shipping_description = $order->getShippingDescription();
		$shipping_description = explode('<br/>',$shipping_description);
		return $shipping_description[0];
	}

	public function getMapSize()
	{
		$height = Mage::getStoreConfig('gls/general/gls_pakkeshop_show_map_height',Mage::app()->getStore()->getId());
		$width = Mage::getStoreConfig('gls/general/gls_pakkeshop_show_map_width',Mage::app()->getStore()->getId());
		return ' style="height:'.$height.'px;width:'.$width.'px;"';
	}
	public function getWrapperSize()
	{
		$width = Mage::getStoreConfig('gls/general/gls_pakkeshop_show_map_width',Mage::app()->getStore()->getId());
		$margin_left = $width/2*-1;	
		return ' style="width:'.$width.'px;margin-left:'.$margin_left.'px;"';
	}

	
	public function setCustomerComment($observer)
	{
		$shippingMethod = explode('_',$observer->getEvent()->getOrder()->getShippingMethod());

		if (sizeof($shippingMethod) > 2) {

			$glsIds = Mage::helper('gls')->getGLSMethodIds('privat');
			$glsPrivatIds = array();
			foreach ($glsIds as $id) {
				array_push($glsPrivatIds,$id['pk']);
			}

			if ($shippingMethod[0] == 'gls' && in_array($shippingMethod[2],$glsPrivatIds)) {

				$glsPrivatKommentar = "";
				if(isset($_SESSION['gls_privat_kommentar']))
				{
					$glsPrivatKommentar = $_SESSION['gls_privat_kommentar'];	
				}
				$glsPrivatKommentar = trim($glsPrivatKommentar);

				if ($glsPrivatKommentar != "") {
					$observer->getEvent()->getOrder()->setGlsPrivatKommentar($glsPrivatKommentar);
				}

			}
			else {
				$observer->getEvent()->getOrder()->setGlsPrivatKommentar('');
			}

		}
	}
	
	public function getLicenseKey()
	{
		return Mage::getStoreConfig('gls/general/gls_serial');
	}
	
	
	public function getGLSMethodIds($type = ''){
		$read = Mage::getSingleton('core/resource')->getConnection('core_read');
		$prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix');
		$ids = $read->fetchAssoc("SELECT pk FROM ".$prefix."shipping_gls WHERE method_code LIKE 'gls_".$type."%'");
		return $ids;
	}

	public function glob_ext_i($exts, $dir = './')
	{
		if (substr($dir, -1) != '/') {
			$dir .= '/';
		}
		$glob_str = $dir . '*.{';
		foreach ($exts as $ext) {
			$glob_str .= strtolower($ext) . ',' . strtoupper($ext) . ',';
		}
		$glob_str = rtrim($glob_str, ',') . '}';

		return glob($glob_str, GLOB_BRACE);
	}

	function __construct()
	{
		$this->temp = Mage::getStoreConfig('gls/general/' . base64_decode('Z2xzX3NlcmlhbA=='));
	}
	
	public function createCsvFileButton()
	{
		$url = Mage::helper("adminhtml")->getUrl("adminhtml/adminhtml_gls_csv/createcsvfiles", array('order_id'=>Mage::registry('current_order')->getId()));
		$onClick = "setLocation('$url')";

		return array(
			'label'  => "Lav GLS csv",
			'onclick'   => $onClick,
		);
	}
}