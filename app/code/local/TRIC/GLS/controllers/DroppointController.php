<?php
class TRIC_GLS_DroppointController extends Mage_Core_Controller_Front_Action
{

	public function indexAction()
	{
		$this->loadLayout(); 
		$block = $this->getLayout()->createBlock('Mage_Core_Block_Template','droppoints',array('template' => 'gls/droppoints.phtml','reference name' => 'left'));
		$this->getLayout()->getBlock('content')->append($block);
		$this->renderLayout();
	}

	public function getDroppointsFromPostalCodeAction()
	{
		$this->getResponse()->setHeader('Content-type', 'text/json; charset=UTF-8');
		$params = $this->getRequest()->getParams();

		if(isset($params['postalCode']) && $params['postalCode'] && isset($params['countryCode']) && $params['countryCode'])
		{	
			$result = Mage::getModel('gls/droppoint')->getDroppointJson($params['postalCode'],$params['countryCode']);
			$this->getResponse()->setBody($result);
		}
		else
		{
			$result = json_encode($params);
			$this->getResponse()->setBody($result);
		}
	}
	
	public function updateAction()
	{
		$event = new Varien_Object();
		$quote = Mage::getModel('checkout/cart')->getQuote();
		$event->setQuote($quote);
		Mage::getModel("gls/observer")->saveData($event);
	}
	
	public function updateAfhenterAction(){
		try {
			$params = $this->getRequest()->getParams();
			if (!isset($params['str']) || $params['str'] == '') return;

			if(trim($params['str']) == ''){
				unset($_SESSION['gls-afhenter']);
			} else {
				$_SESSION['gls-afhenter'] = trim($params['str']);
			}
		} catch(Exception $e) {
			Mage::log($e->getMessage(),null,'droppoints.log');
		}
	}
	
	public function updateTelephoneAction(){
		try {
			$params = $this->getRequest()->getParams();
			if (!isset($params['str']) || $params['str'] == '') return;

			if(trim($params['str']) == ''){
				unset($_SESSION['gls-telephone']);
			} else {
				$_SESSION['gls-telephone'] = trim($params['str']);
			}
		} catch(Exception $e) {
			Mage::log($e->getMessage(),null,'droppoints.log');
		}
	}
	
	function updateGLSPrivatCommentAction(){
		try{
			$params = $this->getRequest()->getParams();
			
			if (!isset($params['str']) || $params['str'] == '') return;

			if(trim($params['str']) == ''){
				unset($_SESSION['gls_privat_kommentar']);
			}
			else{
				$_SESSION['gls_privat_kommentar'] = trim($params['str']);
			}
		}
		catch(Exception $e){
			Mage::log($e->getMessage(),null,'droppoints.log');
		}
	}
	
}