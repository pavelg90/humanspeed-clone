<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2013 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Helper_Rates extends Mage_Core_Helper_Abstract
{
	protected $_tablerates;
    protected $_countries;
    protected $_regions;
    protected $_countriesISO2Codes;
    protected $_countriesISO3Codes;
    protected $_regionsCodes;

	public function getWebsites()
    {
        return Mage::app()->getWebsites();
    }

    public function getDefaultWebsite()
    {
        $website = null;
        $websites = $this->getWebsites();
        if (count($websites)) $website = array_shift($websites);
        return $website;
    }
	
	public function getMethodCodeOptions()
	{
		$options = array();

		$options['gls_pakkeshop'] = $this->__("GLS Pakkeshop");
		$options['gls_erhverv'] = $this->__("GLS Erhverv");
		$options['gls_privat'] = $this->__("GLS Privat");
		$options['gls_international'] = $this->__("GLS International");
		$options['gls_express10'] = $this->__("GLS Express10-Service");
		$options['gls_express12'] = $this->__("GLS Express12-Service");

		return $options;
	}
	
	public function getWebsite()
    {
        $website = null;
        $websiteId = (int) Mage::app()->getFrontController()->getRequest()->getParam('website', 0);
        if ($websiteId) $website = Mage::app()->getWebsite($websiteId);
        if (!$website) $website = $this->getDefaultWebsite();
        return $website;
    }

    public function getWebsiteId($website = null)
    {
        if (is_null($website)) $website = $this->getWebsite();
        return ($website) ? $website->getId() : null;
    }
    
    
    public function getCountries()
    {
        if (is_null($this->_countries)) {
            $countries = array();
            $countryCollection = Mage::getResourceModel('directory/country_collection');
            foreach ($countryCollection as $country) {
                $countries[$country->getId()] = $country;
            }
            $this->_countries = $countries;
        }
        return $this->_countries;
    }

    public function getCountry($countryId)
    {
        $countries = $this->getCountries();
        if (isset($countries[$countryId])) return $countries[$countryId];
        else return null;
    }

    public function getRegions()
    {
        if (is_null($this->_regions)) {
            $regions = array();
            $regionCollection = Mage::getResourceModel('directory/region_collection');
            foreach ($regionCollection as $region) {
                $regions[$region->getId()] = $region;
            }
            $this->_regions = $regions;
        }
        return $this->_regions;
    }

    public function getRegion($regionId)
    {
        $regions = $this->getRegions();
        if (isset($regions[$regionId])) return $regions[$regionId];
        else return null;
    }

    protected function _loadCountriesCodes()
    {
        if (is_null($this->_countriesISO2Codes) || is_null($this->_countriesISO3Codes)) {
            $this->_countriesISO2Codes = $this->_countriesISO3Codes = array();
            foreach ($this->getCountries() as $country) {
                $this->_countriesISO2Codes[$country->getId()] = $country->getIso2Code();
                $this->_countriesISO3Codes[$country->getId()] = $country->getIso3Code();
            }
        }
        return $this;
    }

    protected function _loadRegionsCodes()
    {
        if (is_null($this->_regionsCodes)) {
            $this->_regionsCodes = array();
            foreach ($this->getRegions() as $region) {
                $this->_regionsCodes[$region->getCountryId()][$region->getRegionId()] = $region->getCode();
            }
        }
        return $this;
    }

    public function getCountriesISO2Codes()
    {
        if (is_null($this->_countriesISO2Codes)) $this->_loadCountriesCodes();
        return $this->_countriesISO2Codes;
    }

    public function getCountriesISO3Codes()
    {
        if (is_null($this->_countriesISO3Codes)) $this->_loadCountriesCodes();
        return $this->_countriesISO3Codes;
    }

    public function getRegionsCodes($countryId = null)
    {
        if (is_null($this->_regionsCodes)) $this->_loadRegionsCodes();
        return ($countryId && isset($this->_regionsCodes[$countryId])) ? $this->_regionsCodes[$countryId] : array();
    }
    
}