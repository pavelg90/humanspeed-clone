<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2013 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_GLS_Block_Adminhtml_Rates extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected $_website;

    public function __construct()
    {
        $helper = $this->_getHelper();
        $this->_controller = 'adminhtml_rates';
        $this->_blockGroup = 'gls';
        $this->_headerText = $helper->__('GLS Shipping Rates');
        
        parent::__construct();
        
        $this->setTemplate('gls/gls.phtml');
        
        $this->_updateButton('add', 'label', $helper->__('Add New Shipping Rate'));
    }


    protected function _getHelper()
    {
        return Mage::helper('gls/rates');
    }

    protected function getWebsite()
    {
        if (is_null($this->_website)) $this->_website = $this->_getHelper()->getWebsite();
        return $this->_website;
    }

    public function getWebsiteId()
    {
        return $this->_getHelper()->getWebsiteId($this->getWebsite());
    }
    
    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new', array('website' => $this->getWebsiteId()));
    }
}