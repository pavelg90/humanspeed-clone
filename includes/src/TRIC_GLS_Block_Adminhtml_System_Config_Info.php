<?php

class TRIC_GLS_Block_Adminhtml_System_Config_Info extends Mage_Adminhtml_Block_Abstract implements Varien_Data_Form_Element_Renderer_Interface
{
	public function render(Varien_Data_Form_Element_Abstract $element)
	{
		$module = 'TRIC_GLS';
		
		if (!Mage::getStoreConfig(strtolower($module).'/install/date')) {
			Mage::app()->getConfig()->saveConfig(strtolower($module).'/install/date', time())->reinit();			
		}
		
		$moduleName = (string)Mage::getConfig()->getModuleConfig($module)->name;
		if(!$moduleName){ $moduleName = $module; }
		$moduleVersion = (string)Mage::getConfig()->getModuleConfig($module)->version;

		$html = '';
		
		try {
			if(Mage::app()->getLocale()->getLocaleCode() == 'da_DK') {
				$url = 'http://info.services.tric.dk/extension_dk.html';
			} else {
				$url = 'http://info.services.tric.dk/extension_en.html';
			}
			
			$cacheKey = strtolower($module).'_'.str_replace('.','',$moduleVersion).'_info';
			
			if (!(Mage::app()->loadCache($cacheKey)) || (time() - Mage::app()->loadCache($cacheKey.'_lastcheck')) > 43200) {
				if(extension_loaded('curl')) {
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200);

					$html = curl_exec($ch);

					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					$errorNo = curl_errno($ch);

					curl_close($ch);

					if($httpCode != 200 || $errorNo > 0) {
						return '';
					}
				}

				if(!$html){
					return '';
				}
				
				$html = str_replace('{{NAME}}', $moduleName, $html);
				$html = str_replace('{{VERSION}}', $moduleVersion, $html);

				Mage::app()->saveCache(serialize($html), $cacheKey);
				Mage::app()->saveCache(time(), $cacheKey.'_lastcheck');
				return $html;
			}

			$html = unserialize(Mage::app()->loadCache($cacheKey));
			return $html;
    	
		} catch (Exception $e) {
      		return '';
  		}
    }
}
